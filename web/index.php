<?php

/**
 * Copyright 2016 LINE Corporation
 *
 * LINE Corporation licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

require_once('./LINEBotTiny.php');

$channelAccessToken = getenv('LINE_CHANNEL_ACCESSTOKEN');
$channelSecret = getenv('LINE_CHANNEL_SECRET');

$client = new LINEBotTiny($channelAccessToken, $channelSecret);

///---------------------- code from dice ---------------------------////
function ReplyUnfindMessage($inputStr)
{	
	settype($inputStr, "string");
	error_log("訊息【".$inputStr."】準備以文字訊息回傳");
	
	return 
	array
	(
		array
		(
			'type' => 'text',
			'text' => $inputStr,
		),
	);
	
}

function ReplyGitMessage($keyline)
{	
	error_log("準備回傳旋轉木馬訊息");
	return
	array
	(
		array
		(
			'type' => 'template', 
			'altText' => $keyline["title"], 
			'template' => 
			array
			(
				'type' => 'buttons',
				'thumbnailImageUrl' => $keyline["photourl"], 
				'title' => $keyline["title"], 
				'text' => $keyline["text"], 
				'actions' => 
				array
				(
					array
					(
						'type' => 'uri', // 類型 (連結)
						'label' => '在Git中察看', // 標籤 3
						'uri' => $keyline["url"] // 連結網址
					)
				
				)
			)
		),
		array
		(
			'type' => 'text',
			'text' => $keyline["more"],
		),
	);
}

function ReplyTextMessage($textline)
{	
	return 
	array
	(
		array
		(
			'type' => 'text',
			'text' => $textline["replya"]
		),
		array
		(
			'type' => 'text',
			'text' => $textline["replyb"]
		),
		array
		(
			'type' => 'text',
			'text' => $textline["replyc"]
		),
	);
	
}

function FindinGitJson($message)
{
	$json_reply = file_get_contents("./json/gitreply.json"); 
	$json_data = json_decode($json_reply, true);
	foreach($json_data as $data)
	{
		$keyword = explode(",",$data["keyword"]);// cut keyword by ,
		foreach($keyword as $key)// scan every keyword
		{
			if(strcmp($message,$key) == 0)
			{
				$keyline = 
				array
				(
					"title" => $data["title"],
					"photourl" => $data["photourl"],
					"text" => $data["text"],
					"url"=>$data["url"],
					"keyword"=>$data["keyword"],
					"more"=>$data["more"],
				);
				return $keyline;
			}
			
		}
	}
	return NULL;
}

function FindinTextJson($message)
{
	$json_reply = file_get_contents("./json/textreply.json"); 
	$json_data = json_decode($json_reply, true);
	foreach($json_data as $data)
	{
		$keyword = explode(",",$data["keyword"]);// cut keyword by ,
		foreach($keyword as $key)// scan every keyword
		{
			if(strcmp($message,$key) == 0)
			{
				$textline = 
				array
				(
					"keyword" => $data["keyword"],
					"replya" => $data["replya"],
					"replyb" => $data["replyb"],
					"replyc" => $data["replyc"]
				);
				return $textline;
			}
			
		}
	}
	return NULL;
}

function caseMessage($client, $event)
{
	$message = $event['message'];
	switch ($message['type']) 
	{
		case 'text':
			$m_message = $message['text'];
			$m_message = strtolower($m_message);
			if($m_message!="")
			{
				if(is_null($keyline = FindinGitJson($m_message)) != TRUE)
				{
					$client->replyMessage
					(
						array
						(
							'replyToken' => $event['replyToken'],
							'messages' => ReplyGitMessage($keyline)
							
						)
					);
				}
				else
				{
					if(is_null($textline = FindinTextJson($m_message))!= TRUE)
					{
						$client->replyMessage
						(
							array
							(
								'replyToken' => $event['replyToken'],
								'messages' => ReplyTextMessage($textline)
							)
						);
					}
					else
					{
						$client->replyMessage
						(
							array
							(
								'replyToken' => $event['replyToken'],
								'messages' => ReplyUnfindMessage("對不起我沒有這方面的資訊。請輸入以下關鍵字再試試看: C++ / Java / dft")
							)
						);
					}
				}
			}
			break;
		
	}
}

function caseFollow($client, $event)
{
	error_log("被加入好友");
	$client->replyMessage(
		array(
		'replyToken' => $event['replyToken'],
		'messages' => 
		array
			(
				array
				(
					'type' => 'text',
					'text' => '你好，我是一個介紹Angel的LineBot。請輸入「關於我」來獲得更多資訊吧!'
				),
			)
		)
	);		
}

//-----------------------Code start herer ----------------------------------//
foreach ($client->parseEvents() as $event) 
{
    switch ($event['type']) 
	{
        case 'message':
            caseMessage($client, $event);
            break;
		case 'follow':
			caseFollow($client, $event);
			break;
        default:
            error_log("Unsupporeted event type: " . $event['type']);
            break;
    }
}
?>


